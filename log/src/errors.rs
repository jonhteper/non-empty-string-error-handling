use std::fmt::{Display, Formatter};
use std::io;
use std::num::ParseIntError;
use non_empty_string::ErrorEmptyString;

#[derive(Debug)]
pub enum Error {
    ParseTimestamp(ParseIntError),
    ParseMessage(ErrorEmptyString),
    Io(io::Error),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::ParseTimestamp(err) => write!(f, "Error parsing timestamp: {err}"),
            Error::ParseMessage(err) => write!(f, "Error parsing message: {err}"),
            Error::Io(err) => write!(f, "{err}"),
        }
    }
}

impl std::error::Error for Error {}

impl From<ParseIntError> for Error {
    fn from(err: ParseIntError) -> Self {
        Self::ParseTimestamp(err)
    }
}

impl From<ErrorEmptyString> for Error {
    fn from(err: ErrorEmptyString) -> Self {
        Self::ParseMessage(err)
    }
}


impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Self::Io(err)
    }
}