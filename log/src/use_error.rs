use std::fmt::{Display, Formatter};
use std::fs::File;
use std::fs;
use std::io::Write;

use non_empty_string::NonEmptyString;
pub use crate::errors::Error;


#[derive(Debug)]
pub struct Log {
    timestamp: i64,
    message: NonEmptyString
}

impl Log {
    pub fn new(timestamp: i64, message: NonEmptyString) -> Self {
        Self { timestamp, message }
    }
}

impl Display for Log {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}-{}", self.timestamp, self.message)
    }
}

impl TryFrom<String> for Log {
    type Error = crate::prelude::Error;

    fn try_from(str: String) -> Result<Self, Self::Error> {
        let timestamp_message = str.split("-").collect::<Vec<&str>>();
        Ok(Log {
            timestamp: timestamp_message[0].parse::<i64>()?,
            message: NonEmptyString::try_from(timestamp_message[1].to_string())?,
        })
    }
}



pub struct Logger {
    logs: Vec<Log>,
    file_name: NonEmptyString,
}

impl Logger {
    pub fn new(logs: Vec<Log>, file_name: NonEmptyString) -> Self {
        Self {logs, file_name}
    }

    pub fn print_logs(&self) -> Result<(), Error> {
        let mut file = File::create(self.file_name.as_str())?;
        let content = self.logs.iter()
            .map(|log| log.to_string())
            .collect::<Vec<String>>()
            .join("\n");

        let _ = file.write(content.as_bytes())?;

        Ok(())
    }

    pub fn add_log(&mut self, log: Log) -> Result<(), Error>{
        self.logs.push(log);
        self.print_logs()
    }

    pub fn read_logs_from_file(&mut self) -> Result<(), Error> {
        let file_content = fs::read_to_string(self.file_name.as_str())?;
        let logs_str = file_content.split("\n").collect::<Vec<&str>>();

        for str in logs_str {
            self.logs.push(Log::try_from(str.to_string())?)
        }

        Ok(())
    }
}