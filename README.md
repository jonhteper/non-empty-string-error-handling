# Error handling with libraries

This is an example of why it is necessary to use error types in each library.

To exemplify error handling, we will create a simple Logger in a library and then try to use it in a mini application.
in a mini application.

## The application
We are going to create a logger application that stores logs in three files according to their level of importance:
* the alerts in the `warnings.log` file,
* errors in the `errors.log` file, and
* event logs in `events.log`.

We want the logs to have only a timestamp (in UNIX time) and a message, which should not be empty.
must not be empty. These are simple requirements that could be in a single `crate`, but for learning purposes we will divide them into
learning purposes we are going to divide them into three `crates`.
* One that will contain the logic of the registers.
* Another one that will allow us to use Strings that are not empty.
* The application itself.

First we are going to create our dependencies:
```
$ cargo new log --lib
     Created library `log` package
```
```
$ cargo new non_empty_string --lib
     Created library `log` package     
```

Then we are going to create a new project that will be our application:
```
$ cargo new logger
     Created binary (application) `logger` package
```

Now we are going to write the libraries.