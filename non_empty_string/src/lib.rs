use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, Clone)]
pub struct NonEmptyString(String);

impl Display for NonEmptyString {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

impl NonEmptyString{
    pub fn new(str: String) -> Option<Self> {
        if str.is_empty() {
            return None;
        }
        
        Some(Self(str))
    }

    pub fn as_str(&self) -> &str {
        &self.0
    }

}

#[cfg(not(feature = "use_error"))]
impl TryFrom<String> for NonEmptyString {
    type Error = String;

    fn try_from(str: String) -> Result<Self, Self::Error> {
        Self::new(str).ok_or_else(|| "Error: Empty String".to_string())
    }
}


#[cfg(feature = "use_error")]
#[derive(Debug, Clone)]
pub struct ErrorEmptyString;

#[cfg(feature = "use_error")]
impl Display for ErrorEmptyString {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error: Empty String")
    }
}

#[cfg(feature = "use_error")]
impl std::error::Error for ErrorEmptyString {}

#[cfg(feature = "use_error")]
impl TryFrom<String> for NonEmptyString {
    type Error = ErrorEmptyString;

    fn try_from(str: String) -> Result<Self, ErrorEmptyString> {
       Self::new(str).ok_or_else(|| ErrorEmptyString)
    }
}