# Manejo de errores con librerías

Este es un ejemplo del porqué es necesario utilizar tipos de error en cada librería.

Para ejemplificar el manejo de errores, crearemos un simple Logger en una librería y luego trataremos de usarlo
en una mini aplicación.

## Los requerimientos
Vamos a crear una aplicación de registros que los guarde en tres archivos según su nivel de importancia:
* las alertas en el archivo `warnings.log`,
* los errores en el archivo `errors.log` y
* los registros de eventos en `events.log`

Queremos que los registros tengan solo una marca de tiempo (en tiempo UNIX) y un mensaje, el cual no 
debe estar vacío. Son requerimientos sencillos que podrían estar en un solo `crate`, pero para efectos
de aprendizaje vamos a dividirlos en tres `crates`.
* Uno que contendrá la lógica de los registros.
* Otro que nos permitirá utilizar Strings que no estén vacíos.
* La aplicación en sí.

Primero vamos a crear nuestras dependencias:
```
$ cargo new log --lib
     Created library `log` package
```
```
$ cargo new non_empty_string --lib
     Created library `log` package     
```

Luego vamos a crear un nuevo proyecto que será nuestra aplicación:
```
$ cargo new logger
     Created binary (application) `logger` package
```

Ahora sí, vamos a escribir las librerías.


